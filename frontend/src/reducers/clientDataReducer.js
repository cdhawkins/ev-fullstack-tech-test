const clientDataReducer = (state, action) => {
    switch(action.type) {
        case "FORM INPUTS": 
            return{
                ...state, 
                [action.field] : action.payload
            }
        default: 
            return state;
    }
}

export default clientDataReducer;