import { render, screen } from '@testing-library/react';
import { configure } from "enzyme";
import { h } from 'preact';
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import App from './App';
import Header from './components/Header';
import ClientDataParentComp from './components/ClientDataParentComp';


// App.js:
it("APP JS js renders without crashing", () => {
    <App />;
});

// Issue with JSX testing!!!

// test('APP JS renders learn react link', () => {
//   render(<Header />)
//   const linkElement = screen.getByText(/EV/i)
//   expect(linkElement).toBeInTheDocument();
// });


it("APP JS js renders without crashing", () => {
  <ClientDataParentComp />;
});