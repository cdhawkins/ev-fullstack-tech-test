import React, {
    useReducer
} from "react";
import clientDataReducer from "../reducers/clientDataReducer";
import '../styling/form.css';

const initialFormState = {
    name: "",
    email: "",
    company: "",
    createdDate: Date.now()
};

const FormComp = ({ value }) => {

    const [formState, dispatch] = useReducer(clientDataReducer, initialFormState);

    const handleTextChange = (e) => {
        dispatch({
            type: "FORM INPUTS",
            field: e.target.name,
            payload: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        
        fetch('http://localhost:3001/clients', {
                method: 'POST',
                body: JSON.stringify(formState),
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            .then(res => res.json())
            .then(data => {
                value(previousList => [
                    ...previousList,
                    data ])
            });

        e.target.reset();
    }

    return ( 
    <form className="form-inline" onSubmit = { handleSubmit }>
        <label> Name </label> 
        <input type="text" name="name" required onChange={(e) => handleTextChange(e)}/> 
        <label> Email </label>
        <input  type="email" name="email" required onChange={(e) => handleTextChange(e)}/> 
        <label>Company</label>
        <input type= "text" name="company" required onChange = {(e) => handleTextChange(e)}/>
        <button id="post-submit"> Submit Data </button> 
     </form>
    )


}

export default FormComp;