import React, {useContext} from "react";
import { DataContext } from "./ClientDataParentComp";
import SearchFilter from "./SearchFilter";

const DataTableComp = () => {
  
  const DataReduced = useContext(DataContext);

  const dataLoop = DataReduced.map((a, i) => {
    const newdate = new Date(a.createdDate);
  return(
  <tr key={i}>
    <td component="th" scope="row">{a.name}</td>
    <td>{ a.email }</td>
    <td>{ a.company }</td>
    <td>{ newdate.toDateString() }</td>
  </tr>
    )
  })
    return(
      <>
        <SearchFilter />
        <div>
          <table id="myTable" sx={{ minWidth: 650 }}>
              <tbody>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Company</th>
                <th>Created Date</th>
              </tr>
              {dataLoop}
              </tbody>
          </table>
        </div>
      </>
    )
}

export default DataTableComp;