import React from "react";

const Header = () => {
    return (
        <header className="App-header">
            <h1>EVPro Full-stack Test</h1>
        </header>
    )
}

export default Header;