import React, {
    useState,
    createContext,
    useEffect
} from "react";
import axios from "axios";
import FormComp from "./FormComp";
import DataTableComp from "./DataTableComp";


export const DataContext = createContext();

const ClientDataParentComp = () => {

    const [clientData, setClientData] = useState([])

    useEffect(() => {

        axios.get(`http://localhost:3001/clients`, {
            headers: {
                authorization: "Bearer EVFullStack"
              }
        })
        
            .then(res => {
                const data = res.data;
                setClientData(data);
            }, [clientData])

    }, [setClientData])

    return ( 
        <DataContext.Provider value = { clientData }>   
            <FormComp value = { setClientData }/> 
            <DataTableComp />
        </DataContext.Provider>
    )

}

export default ClientDataParentComp