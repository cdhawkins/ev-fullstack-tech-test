import React from "react";
import Header from "./components/Header.jsx";
import ClientDataParentComp from './components/ClientDataParentComp';
import "./styling/app.css";
import './styling/form.css';
import './styling/table.css';

const App = () => {
  return (
    <div className="App">
      <Header />
      <ClientDataParentComp />
    </div>
  );
};

export default App;
