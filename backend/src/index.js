const express = require('express');
const app = express();
const mongoose = require("mongoose")
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet")
require('dotenv/config');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet());

// Testing access to client:
// app.get('/clients', async (req, res) => {
//   res.send("Clients")
// })

// Import Routes:
const clientRoutes = require("./routes/clients")

// Middleware:
app.use('/clients', clientRoutes)

// Connect to DB:
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true}, (err) => {
  if (err) throw err;
  console.log("connected to DB")
})

app.listen(3001, () => {
  console.log("Server started on port 3001");
});


module.exports = app;