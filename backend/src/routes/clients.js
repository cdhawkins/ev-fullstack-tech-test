const express = require("express");
const router = express.Router();
const Clients = require("../models/Client")
require('dotenv/config');


router.get('/', async (req, res) => {
        // Implemented bearer token - IF you go on 3001 you cannot see data
        if(req.headers.authorization === process.env.BEARER_TOKEN) {
                try {
                        const client = await Clients.find();
                        res.json(client);
                } catch (error) {
                        res.json({message: error});    
                }
        }
        else{
                res.status(401).send('No Access Without Bearer Authorization');
        }
})

router.post('/', async (req, res) => {
        var client = new Clients({
                name: req.body.name,
                email: req.body.email,
                company: req.body.company
        });
        try{
                const savedClient = await client.save();
                res.json(savedClient)
        }catch(err){
                res.json({message: err})   
        }
        
})

router.get('/:name', async (req, res) => {
        try {
                console.log(req.params)
                const client = await Clients.find(req.params);
                res.json(client);
        } catch (error) {
            res.json({message: err});
        }
})


module.exports = router;