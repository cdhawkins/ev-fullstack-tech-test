const mongoose = require("mongoose");

const ClientSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    company:{
        type: String,
        required: true
    },
    createdDate:{
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model("clients", ClientSchema)