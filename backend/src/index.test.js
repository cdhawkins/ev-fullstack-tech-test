const server = require('./index.js');
const supertest = require('supertest');
const requestWithSupertest = supertest(server);
require('dotenv/config');

it('Testing to see if Jest works', () => {
  expect(1).toBe(1)
})

// IDEAL TASKS TO TEST WHEN JSON EXISTS ON ROUTES!
describe('Client Endpoints', () => {
  
  // THIS WILL PASS DUE TO BEARER TOKEN
  it('GET /clients should show all clients', async () => {
      const res = await requestWithSupertest.get('/clients');
      expect(res.status).toEqual(401);
      expect(res.type).toEqual(expect.stringContaining("text/html"));
  });
  
  it('GET /clients/:name should show a client', async () => {
    const res = await requestWithSupertest.get('/clients/john');
    expect(res.type).toEqual(expect.stringContaining('json'));
  });

});
